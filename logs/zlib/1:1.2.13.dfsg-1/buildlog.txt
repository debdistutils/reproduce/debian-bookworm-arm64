+ date
Tue Jul  9 00:19:26 UTC 2024
+ id
uid=0(root) gid=0(root) groups=0(root)
+ pwd
/build/zlib
+ apt-get source --only-source zlib=1:1.2.13.dfsg-1
Reading package lists...
Need to get 1258 kB of source archives.
Get:1 http://deb.debian.org/debian bookworm/main zlib 1:1.2.13.dfsg-1 (dsc) [2399 B]
Get:2 http://deb.debian.org/debian bookworm/main zlib 1:1.2.13.dfsg-1 (tar) [1240 kB]
Get:3 http://deb.debian.org/debian bookworm/main zlib 1:1.2.13.dfsg-1 (diff) [15.7 kB]
dpkg-source: info: extracting zlib in zlib-1.2.13.dfsg
dpkg-source: info: unpacking zlib_1.2.13.dfsg.orig.tar.bz2
dpkg-source: info: unpacking zlib_1.2.13.dfsg-1.debian.tar.xz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: applying cflags-for-minizip
dpkg-source: info: applying use-dso-really
Fetched 1258 kB in 0s (3164 kB/s)
W: Download is performed unsandboxed as root as file 'zlib_1.2.13.dfsg-1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ ls -la
total 1244
drwxr-xr-x  3 root root    4096 Jul  9 00:19 .
drwxr-xr-x  3 root root    4096 Jul  9 00:19 ..
drwxr-xr-x 15 root root    4096 Jul  9 00:19 zlib-1.2.13.dfsg
-rw-r--r--  1 root root   15700 Nov  5  2022 zlib_1.2.13.dfsg-1.debian.tar.xz
-rw-r--r--  1 root root    2399 Nov  5  2022 zlib_1.2.13.dfsg-1.dsc
-rw-r--r--  1 root root 1239825 Nov  5  2022 zlib_1.2.13.dfsg.orig.tar.bz2
+ find . -maxdepth 1 -type f
+ sha256sum ./zlib_1.2.13.dfsg.orig.tar.bz2 ./zlib_1.2.13.dfsg-1.debian.tar.xz ./zlib_1.2.13.dfsg-1.dsc
71feb7947e3c00ef125f83b79a4e529bde31171e5babe48b391f06758d1ab0a1  ./zlib_1.2.13.dfsg.orig.tar.bz2
f66cf3d4f2d7defcd4d1fd1fb0a11ee39f1e01b42ec7d059c9dc5c1695133c44  ./zlib_1.2.13.dfsg-1.debian.tar.xz
3fa1e6b2fc525062aa88207dda52fed8e045373c809d362fe8a2bfbf7cf515a8  ./zlib_1.2.13.dfsg-1.dsc
+ find . -maxdepth 1 -name zlib* -type d
+ cd ./zlib-1.2.13.dfsg
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source zlib=1:1.2.13.dfsg-1
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev bsdextrautils debhelper
  dh-autoreconf dh-strip-nondeterminism dwz gettext gettext-base groff-base
  intltool-debian libarchive-zip-perl libdebhelper-perl libelf1
  libfile-stripnondeterminism-perl libicu72 libpipeline1 libsub-override-perl
  libtool libuchardet0 libxml2 m4 man-db po-debconf
0 upgraded, 26 newly installed, 0 to remove and 0 not upgraded.
Need to get 17.9 MB of archives.
After this operation, 65.6 MB of additional disk space will be used.
Get:1 http://deb.debian.org/debian bookworm/main arm64 gettext-base arm64 0.21-12 [159 kB]
Get:2 http://deb.debian.org/debian bookworm/main arm64 libuchardet0 arm64 0.0.7-1 [67.9 kB]
Get:3 http://deb.debian.org/debian bookworm/main arm64 groff-base arm64 1.22.4-10 [861 kB]
Get:4 http://deb.debian.org/debian bookworm/main arm64 bsdextrautils arm64 2.38.1-5+deb12u1 [86.9 kB]
Get:5 http://deb.debian.org/debian bookworm/main arm64 libpipeline1 arm64 1.5.7-1 [36.4 kB]
Get:6 http://deb.debian.org/debian bookworm/main arm64 man-db arm64 2.11.2-2 [1369 kB]
Get:7 http://deb.debian.org/debian bookworm/main arm64 m4 arm64 1.4.19-3 [276 kB]
Get:8 http://deb.debian.org/debian bookworm/main arm64 autoconf all 2.71-3 [332 kB]
Get:9 http://deb.debian.org/debian bookworm/main arm64 autotools-dev all 20220109.1 [51.6 kB]
Get:10 http://deb.debian.org/debian bookworm/main arm64 automake all 1:1.16.5-1.3 [823 kB]
Get:11 http://deb.debian.org/debian bookworm/main arm64 autopoint all 0.21-12 [495 kB]
Get:12 http://deb.debian.org/debian bookworm/main arm64 libdebhelper-perl all 13.11.4 [81.2 kB]
Get:13 http://deb.debian.org/debian bookworm/main arm64 libtool all 2.4.7-7~deb12u1 [517 kB]
Get:14 http://deb.debian.org/debian bookworm/main arm64 dh-autoreconf all 20 [17.1 kB]
Get:15 http://deb.debian.org/debian bookworm/main arm64 libarchive-zip-perl all 1.68-1 [104 kB]
Get:16 http://deb.debian.org/debian bookworm/main arm64 libsub-override-perl all 0.09-4 [9304 B]
Get:17 http://deb.debian.org/debian bookworm/main arm64 libfile-stripnondeterminism-perl all 1.13.1-1 [19.4 kB]
Get:18 http://deb.debian.org/debian bookworm/main arm64 dh-strip-nondeterminism all 1.13.1-1 [8620 B]
Get:19 http://deb.debian.org/debian bookworm/main arm64 libelf1 arm64 0.188-2.1 [173 kB]
Get:20 http://deb.debian.org/debian bookworm/main arm64 dwz arm64 0.15-1 [101 kB]
Get:21 http://deb.debian.org/debian bookworm/main arm64 libicu72 arm64 72.1-3 [9204 kB]
Get:22 http://deb.debian.org/debian bookworm/main arm64 libxml2 arm64 2.9.14+dfsg-1.3~deb12u1 [619 kB]
Get:23 http://deb.debian.org/debian bookworm/main arm64 gettext arm64 0.21-12 [1248 kB]
Get:24 http://deb.debian.org/debian bookworm/main arm64 intltool-debian all 0.35.0+20060710.6 [22.9 kB]
Get:25 http://deb.debian.org/debian bookworm/main arm64 po-debconf all 1.0.21+nmu1 [248 kB]
Get:26 http://deb.debian.org/debian bookworm/main arm64 debhelper all 13.11.4 [942 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 17.9 MB in 1s (28.3 MB/s)
Selecting previously unselected package gettext-base.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 16360 files and directories currently installed.)
Preparing to unpack .../00-gettext-base_0.21-12_arm64.deb ...
Unpacking gettext-base (0.21-12) ...
Selecting previously unselected package libuchardet0:arm64.
Preparing to unpack .../01-libuchardet0_0.0.7-1_arm64.deb ...
Unpacking libuchardet0:arm64 (0.0.7-1) ...
Selecting previously unselected package groff-base.
Preparing to unpack .../02-groff-base_1.22.4-10_arm64.deb ...
Unpacking groff-base (1.22.4-10) ...
Selecting previously unselected package bsdextrautils.
Preparing to unpack .../03-bsdextrautils_2.38.1-5+deb12u1_arm64.deb ...
Unpacking bsdextrautils (2.38.1-5+deb12u1) ...
Selecting previously unselected package libpipeline1:arm64.
Preparing to unpack .../04-libpipeline1_1.5.7-1_arm64.deb ...
Unpacking libpipeline1:arm64 (1.5.7-1) ...
Selecting previously unselected package man-db.
Preparing to unpack .../05-man-db_2.11.2-2_arm64.deb ...
Unpacking man-db (2.11.2-2) ...
Selecting previously unselected package m4.
Preparing to unpack .../06-m4_1.4.19-3_arm64.deb ...
Unpacking m4 (1.4.19-3) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../07-autoconf_2.71-3_all.deb ...
Unpacking autoconf (2.71-3) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../08-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../09-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../10-autopoint_0.21-12_all.deb ...
Unpacking autopoint (0.21-12) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../11-libdebhelper-perl_13.11.4_all.deb ...
Unpacking libdebhelper-perl (13.11.4) ...
Selecting previously unselected package libtool.
Preparing to unpack .../12-libtool_2.4.7-7~deb12u1_all.deb ...
Unpacking libtool (2.4.7-7~deb12u1) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../13-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libarchive-zip-perl.
Preparing to unpack .../14-libarchive-zip-perl_1.68-1_all.deb ...
Unpacking libarchive-zip-perl (1.68-1) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../15-libsub-override-perl_0.09-4_all.deb ...
Unpacking libsub-override-perl (0.09-4) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../16-libfile-stripnondeterminism-perl_1.13.1-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.1-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../17-dh-strip-nondeterminism_1.13.1-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.1-1) ...
Selecting previously unselected package libelf1:arm64.
Preparing to unpack .../18-libelf1_0.188-2.1_arm64.deb ...
Unpacking libelf1:arm64 (0.188-2.1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../19-dwz_0.15-1_arm64.deb ...
Unpacking dwz (0.15-1) ...
Selecting previously unselected package libicu72:arm64.
Preparing to unpack .../20-libicu72_72.1-3_arm64.deb ...
Unpacking libicu72:arm64 (72.1-3) ...
Selecting previously unselected package libxml2:arm64.
Preparing to unpack .../21-libxml2_2.9.14+dfsg-1.3~deb12u1_arm64.deb ...
Unpacking libxml2:arm64 (2.9.14+dfsg-1.3~deb12u1) ...
Selecting previously unselected package gettext.
Preparing to unpack .../22-gettext_0.21-12_arm64.deb ...
Unpacking gettext (0.21-12) ...
Selecting previously unselected package intltool-debian.
Preparing to unpack .../23-intltool-debian_0.35.0+20060710.6_all.deb ...
Unpacking intltool-debian (0.35.0+20060710.6) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../24-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../25-debhelper_13.11.4_all.deb ...
Unpacking debhelper (13.11.4) ...
Setting up libpipeline1:arm64 (1.5.7-1) ...
Setting up libicu72:arm64 (72.1-3) ...
Setting up bsdextrautils (2.38.1-5+deb12u1) ...
Setting up libarchive-zip-perl (1.68-1) ...
Setting up libdebhelper-perl (13.11.4) ...
Setting up gettext-base (0.21-12) ...
Setting up m4 (1.4.19-3) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-12) ...
Setting up autoconf (2.71-3) ...
Setting up libuchardet0:arm64 (0.0.7-1) ...
Setting up libsub-override-perl (0.09-4) ...
Setting up libelf1:arm64 (0.188-2.1) ...
Setting up libxml2:arm64 (2.9.14+dfsg-1.3~deb12u1) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
update-alternatives: warning: skip creation of /usr/share/man/man1/automake.1.gz because associated file /usr/share/man/man1/automake-1.16.1.gz (of link group automake) doesn't exist
update-alternatives: warning: skip creation of /usr/share/man/man1/aclocal.1.gz because associated file /usr/share/man/man1/aclocal-1.16.1.gz (of link group automake) doesn't exist
Setting up libfile-stripnondeterminism-perl (1.13.1-1) ...
Setting up gettext (0.21-12) ...
Setting up libtool (2.4.7-7~deb12u1) ...
Setting up intltool-debian (0.35.0+20060710.6) ...
Setting up dh-autoreconf (20) ...
Setting up dh-strip-nondeterminism (1.13.1-1) ...
Setting up dwz (0.15-1) ...
Setting up groff-base (1.22.4-10) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up man-db (2.11.2-2) ...
Building database of manual pages ...
Setting up debhelper (13.11.4) ...
Processing triggers for libc-bin (2.36-9+deb12u7) ...
+ env DEB_BUILD_OPTIONS=noautodbgsym nocheck eatmydata dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package zlib
dpkg-buildpackage: info: source version 1:1.2.13.dfsg-1
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Mark Brown <broonie@debian.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture arm64
 debian/rules clean
dh_testdir
dh_testroot
/usr/bin/make distclean
make[1]: Entering directory '/build/zlib/zlib-1.2.13.dfsg'
make -f Makefile.in distclean
make[2]: Entering directory '/build/zlib/zlib-1.2.13.dfsg'
rm -f *.o *.lo *~ \
   example minigzip examplesh minigzipsh \
   example64 minigzip64 \
   infcover \
   libz.* foo.gz so_locations \
   _match.s maketree contrib/infback9/*.o
rm -rf objs
rm -f *.gcda *.gcno *.gcov
rm -f contrib/infback9/*.gcda contrib/infback9/*.gcno contrib/infback9/*.gcov
cp -p zconf.h.in zconf.h
rm -f Makefile zlib.pc configure.log
make[2]: Leaving directory '/build/zlib/zlib-1.2.13.dfsg'
make[1]: Leaving directory '/build/zlib/zlib-1.2.13.dfsg'
rm -f build-stamp configure-stamp foo.gz 
rm -rf debian/64 build64-stamp configure64-stamp
rm -rf debian/32 build32-stamp configure32-stamp
rm -rf debian/n32 buildn32-stamp configuren32-stamp
mv Makefile.stash Makefile
mv: cannot stat 'Makefile.stash': No such file or directory
make: [debian/rules:157: clean] Error 1 (ignored)
mv zlibdefs.h.stash zlibdefs.h
mv: cannot stat 'zlibdefs.h.stash': No such file or directory
make: [debian/rules:158: clean] Error 1 (ignored)
dh_clean 
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building zlib using existing ./zlib_1.2.13.dfsg.orig.tar.bz2
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: building zlib in zlib_1.2.13.dfsg-1.debian.tar.xz
dpkg-source: info: building zlib in zlib_1.2.13.dfsg-1.dsc
 debian/rules build
dh_testdir
if [ ! -f Makefile.stash ]; then cp Makefile Makefile.stash ; fi
AR=ar CC="aarch64-linux-gnu-gcc" CFLAGS="`dpkg-buildflags --get CFLAGS` `dpkg-buildflags --get CPPFLAGS` -Wall -D_REENTRANT -O3" LDFLAGS="`dpkg-buildflags --get LDFLAGS`" uname=GNU ./configure --shared --prefix=/usr --libdir=\${prefix}/lib/aarch64-linux-gnu
Checking for shared library support...
Building shared library libz.so.1.2.13 with aarch64-linux-gnu-gcc.
Checking for size_t... Yes.
Checking for off64_t... Yes.
Checking for fseeko... Yes.
Checking for strerror... Yes.
Checking for unistd.h... Yes.
Checking for stdarg.h... Yes.
Checking whether to use vs[n]printf() or s[n]printf()... using vs[n]printf().
Checking for vsnprintf() in stdio.h... Yes.
Checking for return value of vsnprintf()... Yes.
Checking for attribute(visibility) support... Yes.
touch configure-stamp
dh_testdir
/usr/bin/make
make[1]: Entering directory '/build/zlib/zlib-1.2.13.dfsg'
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN -I. -c -o example.o test/example.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -c -o adler32.o adler32.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -c -o crc32.o crc32.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -c -o deflate.o deflate.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -c -o infback.o infback.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -c -o inffast.o inffast.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -c -o inflate.o inflate.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -c -o inftrees.o inftrees.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -c -o trees.o trees.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -c -o zutil.o zutil.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -c -o compress.o compress.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -c -o uncompr.o uncompr.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -c -o gzclose.o gzclose.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -c -o gzlib.o gzlib.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -c -o gzread.o gzread.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -c -o gzwrite.o gzwrite.c
ar rc libz.a adler32.o crc32.o deflate.o infback.o inffast.o inflate.o inftrees.o trees.o zutil.o compress.o uncompr.o gzclose.o gzlib.o gzread.o gzwrite.o 
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN -o example example.o -Wl,-z,relro -L. libz.a
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN -I. -c -o minigzip.o test/minigzip.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN -o minigzip minigzip.o -Wl,-z,relro -L. libz.a
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -fPIC -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -DPIC -c -o objs/adler32.o adler32.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -fPIC -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -DPIC -c -o objs/crc32.o crc32.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -fPIC -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -DPIC -c -o objs/deflate.o deflate.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -fPIC -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -DPIC -c -o objs/infback.o infback.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -fPIC -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -DPIC -c -o objs/inffast.o inffast.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -fPIC -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -DPIC -c -o objs/inflate.o inflate.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -fPIC -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -DPIC -c -o objs/inftrees.o inftrees.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -fPIC -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -DPIC -c -o objs/trees.o trees.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -fPIC -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -DPIC -c -o objs/zutil.o zutil.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -fPIC -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -DPIC -c -o objs/compress.o compress.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -fPIC -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -DPIC -c -o objs/uncompr.o uncompr.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -fPIC -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -DPIC -c -o objs/gzclose.o gzclose.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -fPIC -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -DPIC -c -o objs/gzlib.o gzlib.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -fPIC -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -DPIC -c -o objs/gzread.o gzread.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -fPIC -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -DPIC -c -o objs/gzwrite.o gzwrite.c
aarch64-linux-gnu-gcc -shared -Wl,-soname,libz.so.1,--version-script,zlib.map -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -fPIC -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN -o libz.so.1.2.13 adler32.lo crc32.lo deflate.lo infback.lo inffast.lo inflate.lo inftrees.lo trees.lo zutil.lo compress.lo uncompr.lo gzclose.lo gzlib.lo gzread.lo gzwrite.lo  -lc -Wl,-z,relro
rm -f libz.so libz.so.1
ln -s libz.so.1.2.13 libz.so
ln -s libz.so.1.2.13 libz.so.1
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN -o examplesh example.o -Wl,-z,relro -L. libz.so.1.2.13
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN -o minigzipsh minigzip.o -Wl,-z,relro -L. libz.so.1.2.13
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN -I. -D_FILE_OFFSET_BITS=64 -c -o example64.o test/example.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN -o example64 example64.o -Wl,-z,relro -L. libz.a
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN -I. -D_FILE_OFFSET_BITS=64 -c -o minigzip64.o test/minigzip.c
aarch64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/zlib/zlib-1.2.13.dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -D_REENTRANT -O3 -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN -o minigzip64 minigzip64.o -Wl,-z,relro -L. libz.a
make[1]: Leaving directory '/build/zlib/zlib-1.2.13.dfsg'
/usr/bin/make test
make[1]: Entering directory '/build/zlib/zlib-1.2.13.dfsg'
hello world
zlib version 1.2.13 = 0x12d0, compile flags = 0xa9
uncompress(): hello, hello!
gzread(): hello, hello!
gzgets() after gzseek:  hello!
inflate(): hello, hello!
large_inflate(): OK
after inflateSync(): hello, hello!
inflate with dictionary: hello, hello!
		*** zlib test OK ***
hello world
zlib version 1.2.13 = 0x12d0, compile flags = 0xa9
uncompress(): hello, hello!
gzread(): hello, hello!
gzgets() after gzseek:  hello!
inflate(): hello, hello!
large_inflate(): OK
after inflateSync(): hello, hello!
inflate with dictionary: hello, hello!
		*** zlib shared test OK ***
hello world
zlib version 1.2.13 = 0x12d0, compile flags = 0xa9
uncompress(): hello, hello!
gzread(): hello, hello!
gzgets() after gzseek:  hello!
inflate(): hello, hello!
large_inflate(): OK
after inflateSync(): hello, hello!
inflate with dictionary: hello, hello!
		*** zlib 64-bit test OK ***
make[1]: Leaving directory '/build/zlib/zlib-1.2.13.dfsg'
touch build-stamp
 debian/rules binary
dh_testdir
dh_testroot
dh_prep
dh_installdirs
/usr/bin/make prefix=/build/zlib/zlib-1.2.13.dfsg/debian/tmp/usr install
make[1]: Entering directory '/build/zlib/zlib-1.2.13.dfsg'
rm -f /build/zlib/zlib-1.2.13.dfsg/debian/tmp/usr/lib/aarch64-linux-gnu/libz.a
cp libz.a /build/zlib/zlib-1.2.13.dfsg/debian/tmp/usr/lib/aarch64-linux-gnu
chmod 644 /build/zlib/zlib-1.2.13.dfsg/debian/tmp/usr/lib/aarch64-linux-gnu/libz.a
cp libz.so.1.2.13 /build/zlib/zlib-1.2.13.dfsg/debian/tmp/usr/lib/aarch64-linux-gnu
chmod 755 /build/zlib/zlib-1.2.13.dfsg/debian/tmp/usr/lib/aarch64-linux-gnu/libz.so.1.2.13
rm -f /build/zlib/zlib-1.2.13.dfsg/debian/tmp/usr/share/man/man3/zlib.3
cp zlib.3 /build/zlib/zlib-1.2.13.dfsg/debian/tmp/usr/share/man/man3
chmod 644 /build/zlib/zlib-1.2.13.dfsg/debian/tmp/usr/share/man/man3/zlib.3
rm -f /build/zlib/zlib-1.2.13.dfsg/debian/tmp/usr/lib/aarch64-linux-gnu/pkgconfig/zlib.pc
cp zlib.pc /build/zlib/zlib-1.2.13.dfsg/debian/tmp/usr/lib/aarch64-linux-gnu/pkgconfig
chmod 644 /build/zlib/zlib-1.2.13.dfsg/debian/tmp/usr/lib/aarch64-linux-gnu/pkgconfig/zlib.pc
rm -f /build/zlib/zlib-1.2.13.dfsg/debian/tmp/usr/include/zlib.h /build/zlib/zlib-1.2.13.dfsg/debian/tmp/usr/include/zconf.h
cp zlib.h zconf.h /build/zlib/zlib-1.2.13.dfsg/debian/tmp/usr/include
chmod 644 /build/zlib/zlib-1.2.13.dfsg/debian/tmp/usr/include/zlib.h /build/zlib/zlib-1.2.13.dfsg/debian/tmp/usr/include/zconf.h
make[1]: Leaving directory '/build/zlib/zlib-1.2.13.dfsg'
install -d debian/tmp/lib/aarch64-linux-gnu
mv debian/tmp/usr/lib/aarch64-linux-gnu/libz.so.* debian/tmp/lib/aarch64-linux-gnu
ln -sf /lib/aarch64-linux-gnu/$(readlink debian/tmp/usr/lib/aarch64-linux-gnu/libz.so) debian/tmp/usr/lib/aarch64-linux-gnu/libz.so
dh_testdir
dh_testroot
dh_installchangelogs -a ChangeLog
dh_installdocs -a
dh_installexamples -a
dh_install -a --sourcedir=debian/tmp
dh_installman -a
dh_lintian -a
dh_link -a
dh_strip -a --dbgsym-migration="zlib1g-dbg (<< 1:1.2.11.dfsg-2~)"
dh_compress -a
dh_fixperms -a
dh_makeshlibs -pzlib1g -V"zlib1g (>= 1:1.2.3.3.dfsg-1)" --add-udeb=zlib1g-udeb
dh_installdeb -a
dh_shlibdeps -a
dh_gencontrol -a
dpkg-gencontrol: warning: package zlib1g-udeb: substitution variable ${shlibs:Depends} unused, but is defined
dh_md5sums -a
dh_builddeb -a
dpkg-deb: building package 'zlib1g' in '../zlib1g_1.2.13.dfsg-1_arm64.deb'.
dpkg-deb: building package 'zlib1g-dev' in '../zlib1g-dev_1.2.13.dfsg-1_arm64.deb'.
dpkg-deb: building package 'zlib1g-udeb' in 'debian/.debhelper/scratch-space/build-zlib1g-udeb/zlib1g-udeb_1.2.13.dfsg-1_arm64.deb'.
	Renaming zlib1g-udeb_1.2.13.dfsg-1_arm64.deb to zlib1g-udeb_1.2.13.dfsg-1_arm64.udeb
 dpkg-genbuildinfo -O../zlib_1.2.13.dfsg-1_arm64.buildinfo
 dpkg-genchanges -O../zlib_1.2.13.dfsg-1_arm64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload (original source is included)
+ date
Tue Jul  9 00:19:44 UTC 2024
+ cd ..
+ + ls zlib1g-dev_1.2.13.dfsg-1_arm64.deb zlib1g_1.2.13.dfsg-1_arm64.deb
sed -e s,_.*,,
+ apt-cache show zlib1g-dev=1:1.2.13.dfsg-1
+ grep ^Version: 
+ sed -e s,^Version: ,,
+ binver=1:1.2.13.dfsg-1
+ binnmuver=1:1.2.13.dfsg-1
+ echo 1:1.2.13.dfsg-1
+ sed s,^[0-9]\+:,,
+ binnmuver=1.2.13.dfsg-1
+ echo 1:1.2.13.dfsg-1
+ sed s,^[0-9]\+:,,
+ srcver=1.2.13.dfsg-1
+ test -f zlib1g-dev_1.2.13.dfsg-1_arm64.deb
+ apt-cache show zlib1g-dev=1.2.13.dfsg-1
+ grep -e ^Filename:  -e ^SHA256: 
+ sed -z s,.*Filename: .*/\(.*\)SHA256: \([0-9a-f]\{64\}\).*,\2  \1,g
+ apt-cache show zlib1g=1:1.2.13.dfsg-1
+ grep ^Version: 
+ sed -e s,^Version: ,,
+ binver=1:1.2.13.dfsg-1
+ binnmuver=1:1.2.13.dfsg-1
+ echo 1:1.2.13.dfsg-1
+ sed s,^[0-9]\+:,,
+ binnmuver=1.2.13.dfsg-1
+ echo 1:1.2.13.dfsg-1
+ sed s,^[0-9]\+:,,
+ srcver=1.2.13.dfsg-1
+ test -f zlib1g_1.2.13.dfsg-1_arm64.deb
+ apt-cache show zlib1g=1.2.13.dfsg-1
+ grep -e+  ^Filename:  -e ^SHA256: 
sed -z s,.*Filename: .*/\(.*\)SHA256: \([0-9a-f]\{64\}\).*,\2  \1,g
+ ls -la
total 2284
drwxr-xr-x  3 root root    4096 Jul  9 00:19 .
drwxr-xr-x  3 root root    4096 Jul  9 00:19 ..
-rw-r--r--  1 root root       0 Jul  9 00:19 SHA256SUMS
drwxr-xr-x 15 root root    4096 Jul  9 00:19 zlib-1.2.13.dfsg
-rw-r--r--  1 root root  912840 Jul  9 00:19 zlib1g-dev_1.2.13.dfsg-1_arm64.deb
-rw-r--r--  1 root root   49424 Jul  9 00:19 zlib1g-udeb_1.2.13.dfsg-1_arm64.udeb
-rw-r--r--  1 root root   82636 Jul  9 00:19 zlib1g_1.2.13.dfsg-1_arm64.deb
-rw-r--r--  1 root root   15708 Jul  9 00:19 zlib_1.2.13.dfsg-1.debian.tar.xz
-rw-r--r--  1 root root    1837 Jul  9 00:19 zlib_1.2.13.dfsg-1.dsc
-rw-r--r--  1 root root    5721 Jul  9 00:19 zlib_1.2.13.dfsg-1_arm64.buildinfo
-rw-r--r--  1 root root    2627 Jul  9 00:19 zlib_1.2.13.dfsg-1_arm64.changes
-rw-r--r--  1 root root 1239825 Nov  5  2022 zlib_1.2.13.dfsg.orig.tar.bz2
+ find . -maxdepth 1 -type f
+ sha256sum ./zlib_1.2.13.dfsg-1_arm64.changes ./SHA256SUMS ./zlib1g-udeb_1.2.13.dfsg-1_arm64.udeb ./zlib1g-dev_1.2.13.dfsg-1_arm64.deb ./zlib_1.2.13.dfsg.orig.tar.bz2 ./zlib1g_1.2.13.dfsg-1_arm64.deb ./zlib_1.2.13.dfsg-1.debian.tar.xz ./zlib_1.2.13.dfsg-1.dsc ./zlib_1.2.13.dfsg-1_arm64.buildinfo
4a1a94226c6fe57ce811b20cf32828000f592523c80057ea953b1c2ccf4243e5  ./zlib_1.2.13.dfsg-1_arm64.changes
e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855  ./SHA256SUMS
e8cc6608a670dabd42c4f2603a33d131a2753a66006a89789fb8379f7a3b214b  ./zlib1g-udeb_1.2.13.dfsg-1_arm64.udeb
afb60f0bae432f2f0edfe2d674e99c1a13a147c9aa2b52f2f23e3df19ece76f5  ./zlib1g-dev_1.2.13.dfsg-1_arm64.deb
71feb7947e3c00ef125f83b79a4e529bde31171e5babe48b391f06758d1ab0a1  ./zlib_1.2.13.dfsg.orig.tar.bz2
70b1e286e9af3f0304981be09d44929cc90f9981f6ff6fa860aa8c8666b71525  ./zlib1g_1.2.13.dfsg-1_arm64.deb
597ea17b033293718f920b63fb24dc5dc17b56754b6736ad8c7bcd38c17b49d8  ./zlib_1.2.13.dfsg-1.debian.tar.xz
0b39064581c8e4cf00a8d032efad000a017455ffbfbbc9fc2911186b4212208b  ./zlib_1.2.13.dfsg-1.dsc
2b8a281e0d3b2b9c85a4d6d009c922fb34764fa5c7de0f43bd68bfeb30d2f275  ./zlib_1.2.13.dfsg-1_arm64.buildinfo
+ tail -v -n+0 SHA256SUMS
==> SHA256SUMS <==
+ sha256sum -c SHA256SUMS
sha256sum: SHA256SUMS: no properly formatted checksum lines found
