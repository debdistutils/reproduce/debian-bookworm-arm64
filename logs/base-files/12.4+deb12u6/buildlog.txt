+ date
Mon Jul  8 23:32:52 UTC 2024
+ id
uid=0(root) gid=0(root) groups=0(root)
+ pwd
/build/base-files
+ apt-get source --only-source base-files=12.4+deb12u6
Reading package lists...
Need to get 67.3 kB of source archives.
Get:1 http://deb.debian.org/debian bookworm/main base-files 12.4+deb12u6 (dsc) [1133 B]
Get:2 http://deb.debian.org/debian bookworm/main base-files 12.4+deb12u6 (tar) [66.1 kB]
dpkg-source: info: extracting base-files in base-files-12.4+deb12u6
dpkg-source: info: unpacking base-files_12.4+deb12u6.tar.xz
Fetched 67.3 kB in 0s (218 kB/s)
W: Download is performed unsandboxed as root as file 'base-files_12.4+deb12u6.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ ls -la
total 84
drwxr-xr-x 3 root root  4096 Jul  8 23:32 .
drwxr-xr-x 3 root root  4096 Jul  8 23:32 ..
drwxr-xr-x 8 root root  4096 Mar  2  2023 base-files-12.4+deb12u6
-rw-r--r-- 1 root root  1133 Mar 29 17:26 base-files_12.4+deb12u6.dsc
-rw-r--r-- 1 root root 66132 Mar 29 17:26 base-files_12.4+deb12u6.tar.xz
+ find . -maxdepth 1 -type f
+ sha256sum ./base-files_12.4+deb12u6.dsc ./base-files_12.4+deb12u6.tar.xz
7e7059826bd34e21b7f0fd3b78b2a2387e23a62c801ea5ee9f5dda82827ecbcd  ./base-files_12.4+deb12u6.dsc
1c837d1a661b11f3c8b386432ae86d87bd6126ef98e43570784d47fbecd6ad09  ./base-files_12.4+deb12u6.tar.xz
+ find . -maxdepth 1 -name base-files* -type d
+ cd ./base-files-12.4+deb12u6
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source base-files=12.4+deb12u6
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev bsdextrautils debhelper
  dh-autoreconf dh-strip-nondeterminism dwz gettext gettext-base groff-base
  intltool-debian libarchive-zip-perl libdebhelper-perl libelf1
  libfile-stripnondeterminism-perl libicu72 libpipeline1 libsub-override-perl
  libtool libuchardet0 libxml2 m4 man-db po-debconf
0 upgraded, 26 newly installed, 0 to remove and 0 not upgraded.
Need to get 17.9 MB of archives.
After this operation, 65.6 MB of additional disk space will be used.
Get:1 http://deb.debian.org/debian bookworm/main arm64 gettext-base arm64 0.21-12 [159 kB]
Get:2 http://deb.debian.org/debian bookworm/main arm64 libuchardet0 arm64 0.0.7-1 [67.9 kB]
Get:3 http://deb.debian.org/debian bookworm/main arm64 groff-base arm64 1.22.4-10 [861 kB]
Get:4 http://deb.debian.org/debian bookworm/main arm64 bsdextrautils arm64 2.38.1-5+deb12u1 [86.9 kB]
Get:5 http://deb.debian.org/debian bookworm/main arm64 libpipeline1 arm64 1.5.7-1 [36.4 kB]
Get:6 http://deb.debian.org/debian bookworm/main arm64 man-db arm64 2.11.2-2 [1369 kB]
Get:7 http://deb.debian.org/debian bookworm/main arm64 m4 arm64 1.4.19-3 [276 kB]
Get:8 http://deb.debian.org/debian bookworm/main arm64 autoconf all 2.71-3 [332 kB]
Get:9 http://deb.debian.org/debian bookworm/main arm64 autotools-dev all 20220109.1 [51.6 kB]
Get:10 http://deb.debian.org/debian bookworm/main arm64 automake all 1:1.16.5-1.3 [823 kB]
Get:11 http://deb.debian.org/debian bookworm/main arm64 autopoint all 0.21-12 [495 kB]
Get:12 http://deb.debian.org/debian bookworm/main arm64 libdebhelper-perl all 13.11.4 [81.2 kB]
Get:13 http://deb.debian.org/debian bookworm/main arm64 libtool all 2.4.7-7~deb12u1 [517 kB]
Get:14 http://deb.debian.org/debian bookworm/main arm64 dh-autoreconf all 20 [17.1 kB]
Get:15 http://deb.debian.org/debian bookworm/main arm64 libarchive-zip-perl all 1.68-1 [104 kB]
Get:16 http://deb.debian.org/debian bookworm/main arm64 libsub-override-perl all 0.09-4 [9304 B]
Get:17 http://deb.debian.org/debian bookworm/main arm64 libfile-stripnondeterminism-perl all 1.13.1-1 [19.4 kB]
Get:18 http://deb.debian.org/debian bookworm/main arm64 dh-strip-nondeterminism all 1.13.1-1 [8620 B]
Get:19 http://deb.debian.org/debian bookworm/main arm64 libelf1 arm64 0.188-2.1 [173 kB]
Get:20 http://deb.debian.org/debian bookworm/main arm64 dwz arm64 0.15-1 [101 kB]
Get:21 http://deb.debian.org/debian bookworm/main arm64 libicu72 arm64 72.1-3 [9204 kB]
Get:22 http://deb.debian.org/debian bookworm/main arm64 libxml2 arm64 2.9.14+dfsg-1.3~deb12u1 [619 kB]
Get:23 http://deb.debian.org/debian bookworm/main arm64 gettext arm64 0.21-12 [1248 kB]
Get:24 http://deb.debian.org/debian bookworm/main arm64 intltool-debian all 0.35.0+20060710.6 [22.9 kB]
Get:25 http://deb.debian.org/debian bookworm/main arm64 po-debconf all 1.0.21+nmu1 [248 kB]
Get:26 http://deb.debian.org/debian bookworm/main arm64 debhelper all 13.11.4 [942 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 17.9 MB in 1s (28.2 MB/s)
Selecting previously unselected package gettext-base.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 16360 files and directories currently installed.)
Preparing to unpack .../00-gettext-base_0.21-12_arm64.deb ...
Unpacking gettext-base (0.21-12) ...
Selecting previously unselected package libuchardet0:arm64.
Preparing to unpack .../01-libuchardet0_0.0.7-1_arm64.deb ...
Unpacking libuchardet0:arm64 (0.0.7-1) ...
Selecting previously unselected package groff-base.
Preparing to unpack .../02-groff-base_1.22.4-10_arm64.deb ...
Unpacking groff-base (1.22.4-10) ...
Selecting previously unselected package bsdextrautils.
Preparing to unpack .../03-bsdextrautils_2.38.1-5+deb12u1_arm64.deb ...
Unpacking bsdextrautils (2.38.1-5+deb12u1) ...
Selecting previously unselected package libpipeline1:arm64.
Preparing to unpack .../04-libpipeline1_1.5.7-1_arm64.deb ...
Unpacking libpipeline1:arm64 (1.5.7-1) ...
Selecting previously unselected package man-db.
Preparing to unpack .../05-man-db_2.11.2-2_arm64.deb ...
Unpacking man-db (2.11.2-2) ...
Selecting previously unselected package m4.
Preparing to unpack .../06-m4_1.4.19-3_arm64.deb ...
Unpacking m4 (1.4.19-3) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../07-autoconf_2.71-3_all.deb ...
Unpacking autoconf (2.71-3) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../08-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../09-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../10-autopoint_0.21-12_all.deb ...
Unpacking autopoint (0.21-12) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../11-libdebhelper-perl_13.11.4_all.deb ...
Unpacking libdebhelper-perl (13.11.4) ...
Selecting previously unselected package libtool.
Preparing to unpack .../12-libtool_2.4.7-7~deb12u1_all.deb ...
Unpacking libtool (2.4.7-7~deb12u1) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../13-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libarchive-zip-perl.
Preparing to unpack .../14-libarchive-zip-perl_1.68-1_all.deb ...
Unpacking libarchive-zip-perl (1.68-1) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../15-libsub-override-perl_0.09-4_all.deb ...
Unpacking libsub-override-perl (0.09-4) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../16-libfile-stripnondeterminism-perl_1.13.1-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.1-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../17-dh-strip-nondeterminism_1.13.1-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.1-1) ...
Selecting previously unselected package libelf1:arm64.
Preparing to unpack .../18-libelf1_0.188-2.1_arm64.deb ...
Unpacking libelf1:arm64 (0.188-2.1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../19-dwz_0.15-1_arm64.deb ...
Unpacking dwz (0.15-1) ...
Selecting previously unselected package libicu72:arm64.
Preparing to unpack .../20-libicu72_72.1-3_arm64.deb ...
Unpacking libicu72:arm64 (72.1-3) ...
Selecting previously unselected package libxml2:arm64.
Preparing to unpack .../21-libxml2_2.9.14+dfsg-1.3~deb12u1_arm64.deb ...
Unpacking libxml2:arm64 (2.9.14+dfsg-1.3~deb12u1) ...
Selecting previously unselected package gettext.
Preparing to unpack .../22-gettext_0.21-12_arm64.deb ...
Unpacking gettext (0.21-12) ...
Selecting previously unselected package intltool-debian.
Preparing to unpack .../23-intltool-debian_0.35.0+20060710.6_all.deb ...
Unpacking intltool-debian (0.35.0+20060710.6) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../24-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../25-debhelper_13.11.4_all.deb ...
Unpacking debhelper (13.11.4) ...
Setting up libpipeline1:arm64 (1.5.7-1) ...
Setting up libicu72:arm64 (72.1-3) ...
Setting up bsdextrautils (2.38.1-5+deb12u1) ...
Setting up libarchive-zip-perl (1.68-1) ...
Setting up libdebhelper-perl (13.11.4) ...
Setting up gettext-base (0.21-12) ...
Setting up m4 (1.4.19-3) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-12) ...
Setting up autoconf (2.71-3) ...
Setting up libuchardet0:arm64 (0.0.7-1) ...
Setting up libsub-override-perl (0.09-4) ...
Setting up libelf1:arm64 (0.188-2.1) ...
Setting up libxml2:arm64 (2.9.14+dfsg-1.3~deb12u1) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
update-alternatives: warning: skip creation of /usr/share/man/man1/automake.1.gz because associated file /usr/share/man/man1/automake-1.16.1.gz (of link group automake) doesn't exist
update-alternatives: warning: skip creation of /usr/share/man/man1/aclocal.1.gz because associated file /usr/share/man/man1/aclocal-1.16.1.gz (of link group automake) doesn't exist
Setting up libfile-stripnondeterminism-perl (1.13.1-1) ...
Setting up gettext (0.21-12) ...
Setting up libtool (2.4.7-7~deb12u1) ...
Setting up intltool-debian (0.35.0+20060710.6) ...
Setting up dh-autoreconf (20) ...
Setting up dh-strip-nondeterminism (1.13.1-1) ...
Setting up dwz (0.15-1) ...
Setting up groff-base (1.22.4-10) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up man-db (2.11.2-2) ...
Building database of manual pages ...
Setting up debhelper (13.11.4) ...
Processing triggers for libc-bin (2.36-9+deb12u7) ...
+ env DEB_BUILD_OPTIONS=noautodbgsym nocheck eatmydata dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package base-files
dpkg-buildpackage: info: source version 12.4+deb12u6
dpkg-buildpackage: info: source distribution bookworm
dpkg-buildpackage: info: source changed by Santiago Vila <sanvila@debian.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture arm64
 debian/rules clean
dh clean
   dh_clean
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building base-files in base-files_12.4+deb12u6.tar.xz
dpkg-source: info: building base-files in base-files_12.4+deb12u6.dsc
 debian/rules build
dh build
   dh_update_autotools_config
   dh_autoreconf
   debian/rules override_dh_auto_build
make[1]: Entering directory '/build/base-files/base-files-12.4+deb12u6'
sh debian/check-md5sum-etc profile
sed -e "s/#VENDORFILE#/debian/g" debian/postinst.in > debian/postinst
make[1]: Leaving directory '/build/base-files/base-files-12.4+deb12u6'
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary
   dh_testroot
   dh_prep
   dh_installdirs
   debian/rules override_dh_auto_install
make[1]: Entering directory '/build/base-files/base-files-12.4+deb12u6'
install -p -m 644 etc/*      debian/base-files/etc
install -p -m 755 motd/*     debian/base-files/etc/update-motd.d
install -p -m 644 licenses/* debian/base-files/usr/share/common-licenses
install -p -m 644 origins/*  debian/base-files/etc/dpkg/origins
install -p -m 644 share/*    debian/base-files/usr/share/base-files
sed -e "s&#OSNAME#&"GNU/`uname | sed -e 's/GNU\///'`"&g" share/motd     > debian/base-files/usr/share/base-files/motd
sed -e "s&#OSNAME#&"GNU/`uname | sed -e 's/GNU\///'`"&g" share/info.dir > debian/base-files/usr/share/base-files/info.dir
sed -e "s&#OSNAME#&"GNU/`uname | sed -e 's/GNU\///'`"&g" etc/issue      > debian/base-files/etc/issue
sed -e "s&#OSNAME#&"GNU/`uname | sed -e 's/GNU\///'`"&g" etc/issue.net  > debian/base-files/etc/issue.net
sed -e "s&#OSNAME#&"GNU/`uname | sed -e 's/GNU\///'`"&g" etc/os-release > debian/base-files/etc/os-release
mv debian/base-files/etc/os-release debian/base-files/usr/lib/os-release
ln -s ../usr/lib/os-release debian/base-files/etc/os-release
make[1]: Leaving directory '/build/base-files/base-files-12.4+deb12u6'
   dh_installdocs
   debian/rules override_dh_installchangelogs
make[1]: Entering directory '/build/base-files/base-files-12.4+deb12u6'
dh_installchangelogs --no-trim
make[1]: Leaving directory '/build/base-files/base-files-12.4+deb12u6'
   dh_installman
   dh_lintian
   dh_perl
   debian/rules override_dh_link
make[1]: Entering directory '/build/base-files/base-files-12.4+deb12u6'
dh_link -X os-release
make[1]: Leaving directory '/build/base-files/base-files-12.4+deb12u6'
   dh_strip_nondeterminism
   debian/rules override_dh_compress
make[1]: Entering directory '/build/base-files/base-files-12.4+deb12u6'
dh_compress -X README
make[1]: Leaving directory '/build/base-files/base-files-12.4+deb12u6'
   debian/rules override_dh_fixperms
make[1]: Entering directory '/build/base-files/base-files-12.4+deb12u6'
dh_fixperms
cd debian/base-files && chown root:staff   var/local
cd debian/base-files && chmod 755  `find . -type d`
cd debian/base-files && chmod 1777 `cat ../1777-dirs`
cd debian/base-files && chmod 2775 `cat ../2775-dirs`
cd debian/base-files && chmod 700 root
make[1]: Leaving directory '/build/base-files/base-files-12.4+deb12u6'
   dh_missing
   dh_dwz -a
   dh_strip -a
   dh_makeshlibs -a
   dh_shlibdeps -a
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'base-files' in '../base-files_12.4+deb12u6_arm64.deb'.
 dpkg-genbuildinfo -O../base-files_12.4+deb12u6_arm64.buildinfo
 dpkg-genchanges -O../base-files_12.4+deb12u6_arm64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Mon Jul  8 23:32:59 UTC 2024
+ cd ..
+ + ls base-files_12.4+deb12u6_arm64.deb
sed -e s,_.*,,
+ apt-cache show base-files=12.4+deb12u6
+ grep ^Version: 
+ sed -e s,^Version: ,,
+ binver=12.4+deb12u6
+ binnmuver=12.4+deb12u6
+ echo 12.4+deb12u6
+ sed s,^[0-9]\+:,,
+ binnmuver=12.4+deb12u6
+ echo 12.4+deb12u6
+ sed s,^[0-9]\+:,,
+ srcver=12.4+deb12u6
+ test -f base-files_12.4+deb12u6_arm64.deb
+ apt-cache show base-files=12.4+deb12u6
+ grep -e ^Filename:  -e ^SHA256: 
+ sed -z s,.*Filename: .*/\(.*\)SHA256: \([0-9a-f]\{64\}\).*,\2  \1,g
+ ls -la
total 172
drwxr-xr-x 3 root root  4096 Jul  8 23:32 .
drwxr-xr-x 3 root root  4096 Jul  8 23:32 ..
-rw-r--r-- 1 root root   100 Jul  8 23:33 SHA256SUMS
drwxr-xr-x 8 root root  4096 Mar  2  2023 base-files-12.4+deb12u6
-rw-r--r-- 1 root root   595 Jul  8 23:32 base-files_12.4+deb12u6.dsc
-rw-r--r-- 1 root root 66132 Jul  8 23:32 base-files_12.4+deb12u6.tar.xz
-rw-r--r-- 1 root root  5200 Jul  8 23:32 base-files_12.4+deb12u6_arm64.buildinfo
-rw-r--r-- 1 root root  1603 Jul  8 23:32 base-files_12.4+deb12u6_arm64.changes
-rw-r--r-- 1 root root 70752 Jul  8 23:32 base-files_12.4+deb12u6_arm64.deb
+ find . -maxdepth 1 -type f
+ sha256sum ./SHA256SUMS ./base-files_12.4+deb12u6_arm64.changes ./base-files_12.4+deb12u6_arm64.deb ./base-files_12.4+deb12u6.dsc ./base-files_12.4+deb12u6.tar.xz ./base-files_12.4+deb12u6_arm64.buildinfo
5139a8e3e50d45794ff1eddf1b2edc60f5385e8b6af8b7c1f1741a67d19cc773  ./SHA256SUMS
8c4105170b7d8835628c2f679dc45664b61439760bafbae8fdd83b5975de9d9a  ./base-files_12.4+deb12u6_arm64.changes
85f1a6ca96ad1eae13ac706a635bf5bdd0d840828c89479222b0dcdda159355b  ./base-files_12.4+deb12u6_arm64.deb
e697ad4612bb2a03e6080668e65a04e7f27770305d54b110c1798042212dcc50  ./base-files_12.4+deb12u6.dsc
1c837d1a661b11f3c8b386432ae86d87bd6126ef98e43570784d47fbecd6ad09  ./base-files_12.4+deb12u6.tar.xz
5b490a82ae53e7873beccb12816e3875093a5328bbddcdaf85d79019a921e67b  ./base-files_12.4+deb12u6_arm64.buildinfo
+ tail -v -n+0 SHA256SUMS
==> SHA256SUMS <==
85f1a6ca96ad1eae13ac706a635bf5bdd0d840828c89479222b0dcdda159355b  base-files_12.4+deb12u6_arm64.deb
+ sha256sum -c SHA256SUMS
base-files_12.4+deb12u6_arm64.deb: OK
+ echo Package base-files version 12.4+deb12u6 is reproducible!
Package base-files version 12.4+deb12u6 is reproducible!
