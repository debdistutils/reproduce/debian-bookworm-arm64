
# Rebuild of Top-popcon Debian GNU/Linux 12.x bookworm on arm64

This project rebuilds
[Top-popcon Debian GNU/Linux 12.x bookworm](https://www.debian.org/releases/bookworm/) on
arm64 in a GitLab pipeline, and publish diffoscope
output for any differences.

## Status

We have rebuilt **32%** of
**Top-popcon Debian GNU/Linux 12.x bookworm**!  That is **32%** of the
packages we have built, and we have built **100%** or
**50** of the **50** source packages to rebuild.

Top-popcon Debian GNU/Linux 12.x bookworm (on arm64) contains binary packages
that corresponds to **50** source packages.  Some binary
packages exists in more than one version, so there is a total of
**50** source packages to rebuild.  Of these we have identical
rebuilds for **16** out of the **50**
builds so far.

We have build logs for **50** rebuilds, which may exceed the
number of total source packages to rebuild when a particular source
package (or source package version) has been removed from the archive.
Of the packages we built, **16** packages could be rebuilt
identically, and there are **32** packages that we could
not rebuilt identically.  Building **2** package had build
failures.  We do not attempt to build **0** packages.

[[_TOC_]]

### Rebuildable packages

The following **16** packages could be built locally to
produce the exact same package that is shipped in the archive.

| Package | Version | Build log | Build job |
| ------- | ------- | --------- | --------- |
| acl | 2.3.1-3 | [build Mon Jul  8 23:54:08 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/acl/2.3.1-3/buildlog.txt) | [job 7291796735](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291796735) |
| adduser | 3.134 | [build Mon Jul  8 23:58:18 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/adduser/3.134/buildlog.txt) | [job 7291804714](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291804714) |
| base-files | 12.4+deb12u6 | [build Mon Jul  8 23:32:52 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/base-files/12.4+deb12u6/buildlog.txt) | [job 7291736732](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291736732) |
| debconf | 1.5.82 | [build Mon Jul  8 23:44:15 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/debconf/1.5.82/buildlog.txt) | [job 7291770253](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291770253) |
| debian-archive-keyring | 2023.3+deb12u1 | [build Tue Jul  9 19:23:14 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/debian-archive-keyring/2023.3+deb12u1/buildlog.txt) | [job 7301201746](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7301201746) |
| dpkg | 1.21.22 | [build Mon Jul  8 23:28:37 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/dpkg/1.21.22/buildlog.txt) | [job 7291717210](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291717210) |
| glibc | 2.36-9+deb12u7 | [build Tue Jul  9 00:19:53 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/glibc/2.36-9+deb12u7/buildlog.txt) | [job 7291894155](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291894155) |
| grep | 3.8-5 | [build Tue Jul  9 00:03:38 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/grep/3.8-5/buildlog.txt) | [job 7291841254](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291841254) |
| netbase | 6.4 | [build Mon Jul  8 23:40:12 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/netbase/6.4/buildlog.txt) | [job 7291759165](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291759165) |
| pam | 1.5.2-6+deb12u1 | [build Mon Jul  8 23:32:27 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/pam/1.5.2-6+deb12u1/buildlog.txt) | [job 7291736728](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291736728) |
| readline | 8.2-1.3 | [build Mon Jul  8 23:49:54 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/readline/8.2-1.3/buildlog.txt) | [job 7291786217](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291786217) |
| slang2 | 2.3.3-3 | [build Mon Jul  8 23:54:47 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/slang2/2.3.3-3/buildlog.txt) | [job 7291796738](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291796738) |
| sysvinit | 3.06-4 | [build Mon Jul  8 23:19:16 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/sysvinit/3.06-4/buildlog.txt) | [job 7291681888](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291681888) |
| tzdata | 2024a-0+deb12u1 | [build Mon Jul  8 23:33:15 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/tzdata/2024a-0+deb12u1/buildlog.txt) | [job 7291736733](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291736733) |
| ucf | 3.0043+nmu1 | [build Tue Jul  9 19:23:11 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/ucf/3.0043+nmu1/buildlog.txt) | [job 7301201744](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7301201744) |
| util-linux | 2.38.1-5+deb12u1 | [build Mon Jul  8 23:28:16 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/util-linux/2.38.1-5+deb12u1/buildlog.txt) | [job 7291717211](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291717211) |

### Build failures

The following **2** packages have build failures, making it
impossible to even compare the binary package in the archive with what
we are able to build locally.

| Package | Version | Build log | Build job |
| ------- | ------- | --------- | --------- |
| coreutils | 9.1-1 | [build Mon Jul  8 23:19:11 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/coreutils/9.1-1/buildlog.txt) | [job 7291681885](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291681885) |
| tar | 1.34+dfsg-1.2+deb12u1 | [build Mon Jul  8 23:49:41 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/tar/1.34+dfsg-1.2+deb12u1/buildlog.txt) | [job 7291786215](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291786215) |

### Unrebuildable packages

The following **32** packages unfortunately differ in
some way compared to the version distributed in the archive.  Please
investigate the build log and help us improve things!

| Package | Version | Build log | Jobs | Diffoscope |
| ------- | ------- | --------- | ---- | ---------- |
| apt | 2.6.1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/apt/2.6.1/buildlog.txt) | build [7291768697](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291768697) <br> diff [7291768706](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291768706) | [diff 7291768706](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291768706/artifacts/diffoscope/index.html) |
| attr | 1:2.5.1-4 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/attr/1:2.5.1-4/buildlog.txt) | build [7291759161](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291759161) <br> diff [7291759183](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291759183) | [diff 7291759183](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291759183/artifacts/diffoscope/index.html) |
| base-passwd | 3.6.1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/base-passwd/3.6.1/buildlog.txt) | build [7291786216](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291786216) <br> diff [7291786221](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291786221) | [diff 7291786221](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291786221/artifacts/diffoscope/index.html) |
| bash | 5.2.15-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/bash/5.2.15-2/buildlog.txt) | build [7301201745](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7301201745) <br> diff [7301201753](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7301201753) | [diff 7301201753](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7301201753/artifacts/diffoscope/index.html) |
| bzip2 | 1.0.8-5 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/bzip2/1.0.8-5/buildlog.txt) | build [7291768694](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291768694) <br> diff [7291768702](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291768702) | [diff 7291768702](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291768702/artifacts/diffoscope/index.html) |
| cpio | 2.13+dfsg-7.1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/cpio/2.13+dfsg-7.1/buildlog.txt) | build [7291768698](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291768698) <br> diff [7291768709](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291768709) | [diff 7291768709](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291768709/artifacts/diffoscope/index.html) |
| e2fsprogs | 1.47.0-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/e2fsprogs/1.47.0-2/buildlog.txt) | build [7291804717](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291804717) <br> diff [7291804722](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291804722) | [diff 7291804722](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291804722/artifacts/diffoscope/index.html) |
| expat | 2.5.0-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/expat/2.5.0-1/buildlog.txt) | build [7291681891](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291681891) <br> diff [7291681903](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291681903) | [diff 7291681903](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291681903/artifacts/diffoscope/index.html) |
| findutils | 4.9.0-4 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/findutils/4.9.0-4/buildlog.txt) | build [7291796736](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291796736) <br> diff [7291796741](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291796741) | [diff 7291796741](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291796741/artifacts/diffoscope/index.html) |
| gettext | 0.21-12 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/gettext/0.21-12/buildlog.txt) | build [7291681893](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291681893) <br> diff [7291681908](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291681908) | [diff 7291681908](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291681908/artifacts/diffoscope/index.html) |
| gnupg2 | 2.2.40-1.1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/gnupg2/2.2.40-1.1/buildlog.txt) | build [7291736729](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291736729) <br> diff [7291736739](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291736739) | [diff 7291736739](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291736739/artifacts/diffoscope/index.html) |
| gzip | 1.12-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/gzip/1.12-1/buildlog.txt) | build [7294448909](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7294448909) <br> diff [7294448920](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7294448920) | [diff 7294448920](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7294448920/artifacts/diffoscope/index.html) |
| hostname | 3.23+nmu1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/hostname/3.23+nmu1/buildlog.txt) | build [7291804716](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291804716) <br> diff [7291804721](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291804721) | [diff 7291804721](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291804721/artifacts/diffoscope/index.html) |
| iputils | 3:20221126-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/iputils/3:20221126-1/buildlog.txt) | build [7291717214](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291717214) <br> diff [7291717219](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291717219) | [diff 7291717219](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291717219/artifacts/diffoscope/index.html) |
| keyutils | 1.6.3-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/keyutils/1.6.3-2/buildlog.txt) | build [7291717213](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291717213) <br> diff [7291717218](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291717218) | [diff 7291717218](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291717218/artifacts/diffoscope/index.html) |
| libcap2 | 1:2.66-4 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/libcap2/1:2.66-4/buildlog.txt) | build [7291786218](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291786218) <br> diff [7291786223](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291786223) | [diff 7291786223](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291786223/artifacts/diffoscope/index.html) |
| libedit | 3.1-20221030-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/libedit/3.1-20221030-2/buildlog.txt) | build [7291841257](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291841257) <br> diff [7291841277](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291841277) | [diff 7291841277](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291841277/artifacts/diffoscope/index.html) |
| libgpg-error | 1.46-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/libgpg-error/1.46-1/buildlog.txt) | build [7291681886](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291681886) <br> diff [7291681897](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291681897) | [diff 7291681897](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291681897/artifacts/diffoscope/index.html) |
| liblocale-gettext-perl | 1.07-5 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/liblocale-gettext-perl/1.07-5/buildlog.txt) | build [7301201747](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7301201747) <br> diff [7301201756](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7301201756) | [diff 7301201756](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7301201756/artifacts/diffoscope/index.html) |
| libselinux | 3.4-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/libselinux/3.4-1/buildlog.txt) | build [7291717212](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291717212) <br> diff [7291717217](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291717217) | [diff 7291717217](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291717217/artifacts/diffoscope/index.html) |
| logrotate | 3.21.0-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/logrotate/3.21.0-1/buildlog.txt) | build [7301309617](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7301309617) <br> diff [7301309625](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7301309625) | [diff 7301309625](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7301309625/artifacts/diffoscope/index.html) |
| lvm2 | 2.03.16-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/lvm2/2.03.16-2/buildlog.txt) | build [7291804718](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291804718) <br> diff [7291804723](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291804723) | [diff 7291804723](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291804723/artifacts/diffoscope/index.html) |
| mawk | 1.3.4.20200120-3.1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/mawk/1.3.4.20200120-3.1/buildlog.txt) | build [7291759172](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291759172) <br> diff [7291759202](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291759202) | [diff 7291759202](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291759202/artifacts/diffoscope/index.html) |
| ncurses | 6.4-4 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/ncurses/6.4-4/buildlog.txt) | build [7291940850](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291940850) <br> diff [7291940856](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291940856) | [diff 7291940856](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291940856/artifacts/diffoscope/index.html) |
| newt | 0.52.23-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/newt/0.52.23-1/buildlog.txt) | build [7291736735](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291736735) <br> diff [7291736744](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291736744) | [diff 7291736744](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291736744/artifacts/diffoscope/index.html) |
| perl | 5.36.0-7+deb12u1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/perl/5.36.0-7+deb12u1/buildlog.txt) | build [7291841246](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291841246) <br> diff [7291841265](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291841265) | [diff 7291841265](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291841265/artifacts/diffoscope/index.html) |
| popt | 1.19+dfsg-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/popt/1.19+dfsg-1/buildlog.txt) | build [7291879710](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291879710) <br> diff [7291879715](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291879715) | [diff 7291879715](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291879715/artifacts/diffoscope/index.html) |
| popularity-contest | 1.76 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/popularity-contest/1.76/buildlog.txt) | build [7291759170](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291759170) <br> diff [7291759194](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291759194) | [diff 7291759194](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291759194/artifacts/diffoscope/index.html) |
| procps | 2:4.0.2-3 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/procps/2:4.0.2-3/buildlog.txt) | build [7291796737](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291796737) <br> diff [7291796742](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291796742) | [diff 7291796742](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291796742/artifacts/diffoscope/index.html) |
| sed | 4.9-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/sed/4.9-1/buildlog.txt) | build [7291768696](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291768696) <br> diff [7291768704](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291768704) | [diff 7291768704](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291768704/artifacts/diffoscope/index.html) |
| shadow | 1:4.13+dfsg1-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/shadow/1:4.13+dfsg1-1/buildlog.txt) | build [7291879709](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291879709) <br> diff [7291879714](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291879714) | [diff 7291879714](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291879714/artifacts/diffoscope/index.html) |
| zlib | 1:1.2.13.dfsg-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/blob/main/logs/zlib/1:1.2.13.dfsg-1/buildlog.txt) | build [7291894154](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291894154) <br> diff [7291894159](https://gitlab.com/debdistutils/reproduce/debian-bookworm-arm64/-/jobs/7291894159) | [diff 7291894159](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-arm64/-/jobs/7291894159/artifacts/diffoscope/index.html) |

### Timestamps

The timestamps of the archives used as input to find out which
packages to reproduce are as follows.  To be precise, these are the
`Date` field in the respectively `Release` file used to construct
the list of packages to evaluate.

When speaking about "current" status of this effort it makes sense to
use the latest timestamp from the set below, which is
**Mon Jul  8 20:22:01 UTC 2024**.

| Archive/Suite | Timestamp |
| ------------- | --------- |
| debian/bookworm | Sat, 29 Jun 2024 09:06:14 UTC |
| debian/bookworm-updates | Mon, 08 Jul 2024 20:22:01 UTC |
| debian-security/bookworm-security | Sun, 07 Jul 2024 12:12:17 UTC |

## License

The content of this repository is automatically generated by
[debdistrebuild](https://gitlab.com/debdistutils/debdistrebuild),
which is published under the
[AGPLv3+](https://www.gnu.org/licenses/agpl-3.0.en.html) and to the
extent the outputs are copyrightable they are released under the same
license.

## Contact

The maintainer of this project is [Simon
Josefsson](https://blog.josefsson.org/).

